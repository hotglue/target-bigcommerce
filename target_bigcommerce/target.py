from target_hotglue.target import TargetHotglue
from singer_sdk import typing as th

from target_bigcommerce.sinks import (
    SalesOrdersSink,
    UpdateInventorySink
)


class TargetBigCommerce(TargetHotglue):
    SINK_TYPES = [SalesOrdersSink, UpdateInventorySink]
    MAX_PARALLELISM = 10
    name = "target-bigcommerce"


if __name__ == "__main__":
    TargetBigCommerce.cli()
