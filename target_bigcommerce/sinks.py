import json
import pycountry
import requests

import operator
from .client import BigCommerceSink

class SalesOrdersSink(BigCommerceSink):
    name = "SalesOrders"
    endpoint = "/v2/orders"
    method = "POST"

    def map_country(self, possible_country_name):
        if possible_country_name is None:
            return None, None

        country = pycountry.countries.get(name=possible_country_name)
        if country:
            return country, country.alpha_2

        country = pycountry.countries.get(alpha_2=possible_country_name)
        if country:
            return country.name, possible_country_name

        country = pycountry.countries.get(alpha_3=possible_country_name)
        if country:
            return country.name, country.alpha_2

    def one_liner_address(self, customer_address: dict):
        lines = [
            customer_address[x] for x in customer_address.keys()
            if x in ["line1", "line2", "line3"] and
            customer_address[x] != "" and
            customer_address[x] is not None
        ]
        return " ".join(lines)

    def map_record(self, record):
        first_name = None
        last_name = None

        if record.get("customer_name"):
            first_name, *last_name = record["customer_name"].split(" ")
            last_name = " ".join(last_name)

        customer_address = record.get("billing_address") or dict()
        shipping_address = record.get("shipping_address") or dict()

        if isinstance(customer_address, str):
            customer_address = json.loads(customer_address)

        if isinstance(shipping_address, str):
            shipping_address = json.loads(shipping_address)

        country, country_alpha2 = self.map_country(customer_address.get("country"))
        shipping_country, shipping_country_alpha2 = self.map_country(shipping_address.get("country"))

        products = []
        for item in record.get("line_items", []):
            product = {}
            if item.get("product_id"):
                if isinstance(item["product_id"], str):
                    item["product_id"] = int(item["product_id"])

                product["product_id"] = item.get("product_id")

            elif item.get("product_name") is not None and item.get("product_id") is None:
                self.logger.info(f"Getting product id for {item.get('product_name')}")
                product_id = self.get_product_id_by_name(item.get("product_name"))

                if product_id is None:
                    product["name"] = item.get("product_name")

                else:
                    product["product_id"] = product_id

            product["quantity"] = item.get("quantity")
            product["price_inc_tax"] = item.get("total_price")
            product["price_ex_tax"] = item.get("total_price") - item.get("tax_amount")
            products.append(product)

        billing_address_one_line = self.one_liner_address(customer_address)

        customer_id = self.get_customer_by_email(record.get("customer_email"))
        # NOTE: We are only creating the customer if an email is provided
        if customer_id is None and record.get("customer_email") is not None:
            customer_id = self.create_new_customer(
                first_name = first_name,
                last_name = last_name,
                email = record.get("customer_email"),
                address = billing_address_one_line,
                city = customer_address.get("city"),
                country_code = country_alpha2
            )

        record_mapping = {
            "status_id": self.get_possible_order_status(record.get("status")),
            "customer_id": customer_id,
            "billing_address": {
                "first_name": first_name,
                "last_name": last_name,
                "street_1": billing_address_one_line,
                "city": customer_address.get("city"),
                "state": customer_address.get("state"),
                "zip": customer_address.get("postal_code"),
                "country": country,
                "country_iso2": country_alpha2,
                "email": record.get("customer_email"),
            },
            "shipping_addresses": [{
                "first_name": first_name,
                "last_name": last_name,
                "street_1": self.one_liner_address(shipping_address),
                "city": shipping_address.get("city"),
                "state": shipping_address.get("state"),
                "zip": shipping_address.get("postal_code"),
                "country": shipping_country,
                "country_iso2": shipping_country_alpha2,
                "email": record.get("customer_email"),
            }],
            "products": products
        }

        if record.get("id"):
            record_mapping["id"] = record.get("id")

        elif record.get("order_number"):
            record_mapping["id"] = record.get("order_number")

        return record_mapping

    def product_diff(self, record):
        order_id = record.get("id")
        products = self.get_order_products(order_id)

        if products:
            self.method = "PUT"
            products_dict = {}
            products_to_be_removed = []
            for product in products:
                products_dict[product["product_id"]] = {
                    "quantity": product["quantity"],
                    "price_inc_tax": product["price_inc_tax"],
                    "price_ex_tax": product["price_ex_tax"]
                }

            for idx, product in enumerate(record.get("products", [])):
                if product["product_id"] in products_dict.keys():
                    if product["quantity"] != products_dict[product["product_id"]]["quantity"]:
                        diff = product["quantity"] - products_dict[product["product_id"]]["quantity"]
                        if diff > 0:
                            product["quantity"] = product["quantity"] - diff
                        else:
                            product["quantity"] = product["quantity"] + diff
                    else:
                        products_to_be_removed.append(idx)

            for idx in products_to_be_removed:
                record["products"].pop(idx)

            return record
        else: # Switch method to POST and return original record to create a new SalesOrder if one with the ID provided does not exist
            self.method = "POST"
            return record

    def create_order_shipment(self, record):
        order_id = record.pop("id")
        url = self.url(f"/v2/orders/{order_id}/shipments")
        payload = record
        response = requests.post(
            url,
            headers=self.client.headers,
            json=payload
        )
        if response.status_code > 203:
            response.raise_for_status()

        return response.json()

    def get_or_create_order_shipment(self, record):
        shipments = self.get_order_shipments(record.get("id"))
        if shipments is None:
            return self.create_order_shipment(record)

        for shipment in shipments:
            if shipment.get("tracking_number") == record.get("tracking_number"):
                return shipment

        return self.create_order_shipment(record)

    def preprocess_record(self, record: dict, context: dict) -> None:
        mapped_record = self.map_record(record)

        if mapped_record.get("id"):
            mapped_record = self.product_diff(mapped_record)

        if record.get("tracking_number"):
            mapped_record["tracking_number"] = record.get("tracking_number")
            mapped_record["carrier"] = record.get("carrier")

        return mapped_record

    def upsert_record(self, record: dict, context: dict):
        state_updates = {}
        self.logger.info(f"Sending Sales Orders request to BigCommerce with payload: {record}")
        tracking_number = None

        if record.get("tracking_number"):
            tracking_number = record.pop("tracking_number")

        if record.get("carrier"):
            carrier = record.pop("carrier")

        if self.method == "PUT":
            order_id = record.pop("id")

            # If there's no status id, we can just update the fulfillment status below
            if record.get("status_id") is not None:
                order_response = self.client.put(self.url(f"{self.endpoint}/{order_id}"), json=record).json()
            else:
                order_response = {"id": order_id}
        else:
            order_response = self.client.post(self.url(self.endpoint), json=record).json()
            order_id = order_response.get("id")

        if tracking_number:
            shipping_address_id = self.get_order_shipping_addresses(order_id)[0].get("id")
            items = self.get_order_products_ids(order_id)
            self.get_or_create_order_shipment({
                "id": order_id,
                "order_address_id": shipping_address_id,
                "tracking_number": tracking_number,
                "tracking_carrier": carrier,
                "items": items
            })

        return order_response, True, state_updates


class UpdateInventorySink(BigCommerceSink):
    name = "UpdateInventory"
    endpoint = "/v3/catalog/products"
    method = "PUT"

    def set_operator(self, old_value, new_value):
        return new_value

    def preprocess_record(self, record: dict, context: dict) -> None:
        operations = {
            "add": operator.add,
            "subtract": operator.sub,
            "set": self.set_operator
        }

        product = self.get_products_by_sku(record.get("sku"))

        if not product:
            return {"error": True}

        if product.get("error"):
            return product
        if product.get("inventory_tracking") == "none":
            self.logger.info(f"Product with sku {record.get('sku')} doesn't have an inventory tracking, skipping.")
            return {"id": product.get("id"), "skip": True}
        if not product.get("is_variant"):
            product_to_update = {
                "id": product.get("id"),
                "inventory_level": operations[record.get("operation")](
                    product["inventory_level"],
                    int(record.get("quantity"))
                )
            }
            return product_to_update
        else:
            product_to_update = {
                "id": product.get("product_id"),
                "variant_id": product.get("id"),
                "is_variant": True,
                "inventory_level": operations[record.get("operation")](
                    product["inventory_level"],
                    int(record.get("quantity"))
                )
            }
            return product_to_update


    def upsert_record(self, record: dict, context: dict):
        # note: this is a batch endpoint but it's being treated as a single record endpoint to keep track
        # in the state of the products' inventory that is being updated
        state_updates = {}
        self.logger.info(f"Sending Update Inventory request to BigCommerce with payload: {record}")

        if record.get("error"):
            return "", False, state_updates

        if record.get("is_variant"):
            self.logger.info(f"Sending Update Inventory to product variant")
            record.pop("id")
            record.pop("is_variant")
            record["id"] = record.pop("variant_id")
            product_response = self.client.put(self.url(f"/v3/catalog/variants"), json=[record])
            variant_id = product_response.json().get("data", [{}])[0].get("id")
            return variant_id, True, state_updates

        if not record.get("skip"):
            product_response = self.client.put(self.url(self.endpoint), json=[record])
            product_id = product_response.json().get("id")
            return product_id, True, state_updates

        return record.get("id"), True, {"skip": "skipping product due to not set inventory_tracking"}
