import requests
import urllib.parse

from target_hotglue.client import HotglueSink
from target_bigcommerce.bigcommerce import BigCommerceClient

class BigCommerceSink(HotglueSink):
    @property
    def base_url(self):
        return f"https://api.bigcommerce.com/stores/{self.config.get('store_hash')}"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = BigCommerceClient(
            client_id= self.config.get("client_id"),
            access_token= self.config.get("access_token"),
            store_hash= self.config.get("store_hash"),
        )

    def get_customer_by_email(self, email):
        url = self.url(f"/v3/customers?email:in={email}")
        response = requests.get(url, headers=self.client.headers)
        if response.status_code != 200:
            self.logger.info(f"Skipping customer with email: {email}... Customer not found on API, considering it as a new customer")
            return None

        resp = response.json()
        if len(resp.get("data", [])) == 0:
            self.logger.info(f"Skipping customer with email: {email}... Customer not found on API, considering it as a new customer")
            return None

        return resp.get('data')[0].get("id")

    def get_product_id_by_name(self, name):
        url = self.url(f"/v3/catalog/products?name={name}")
        response = requests.get(url, headers=self.client.headers)
        if response.status_code != 200:
            self.logger.info(f"Skipping product named: {name}... Product not found on API, considering it as a custom product")
            return None
        resp = response.json()

        if len(resp.get("data")) == 0:
            self.logger.info(f"Skipping product named: {name}... Product not found on API, considering it as a custom product")
            return None

        return resp.get("data")[0].get("id")

    def create_new_customer(self, first_name, last_name, email, address, city, country_code, address_type="residential"):
        url = self.url("/v3/customers")
        payload = [{
            "first_name": first_name,
            "last_name": last_name,
            "email": email
        }]
        response = requests.post(
            url,
            headers=self.client.headers,
            json=payload
        )
        if response.status_code > 203:
            response.raise_for_status()

        return response.json().get("data")[0].get("id")

    def get_possible_order_status(self, order_status):
        if order_status is None:
            return None

        resp = self.client.get(self.url("/v2/order_statuses"))

        if resp.status_code != 200:
            resp.raise_for_status()

        possible_statuses = resp.json()
        for status in possible_statuses:
            if status.get("name", "").lower() == order_status.lower():
                return status.get("id")

        return possible_statuses[0].get("id")

    def get_order_by_id(self, order_id):
        url = self.url(f"/v2/orders/{order_id}")
        resp = self.client.get(url)
        if resp.status_code == 404:
            return None

        return resp.json()

    def get_order_products(self, order_id):
        url = self.url(f"/v2/orders/{order_id}/products")
        try:
            resp = self.client.get(url)
            return resp.json()
        except: #need this here or whole job will fail on a 404
            return None

    def get_products_by_sku(self, sku):
        url_sku = urllib.parse.quote(sku)
        url = self.url(f"/v3/catalog/products?sku={url_sku}")
        resp = self.client.get(url)

        if resp.status_code != 200:
            self.logger.info(f"Request to fetch product with sku {sku} failed with {resp.text}")
            return {"error": resp.text}

        content = resp.json()
        if len(content.get("data", [])) > 0:
            return content["data"][0]

        self.logger.info(f"Product with sku {sku} was not found in this account, checking for variants.")

        find_variant = True
        original_url = "/v3/catalog/variants"
        variants_list = self.url()
        while find_variant:
            resp = self.client.get(variants_list)
            content = resp.json()

            if resp.status_code != 200:
                self.logger.info(f"Request to fetch variants failed with {resp.text}")
                return {"error": resp.text}

            for variant in content.get("data", []):
                if variant.get("sku").lower() == sku.lower():
                    variant["is_variant"] = True
                    return variant

            if content.get("meta", {}).get("pagination", {}).get("links", {}).get("next"):
                next_page = content.get("meta", {}).get("pagination", {}).get("links", {}).get("next")
                variants_list = self.url(original_url + next_page)
            else:
                find_variant = False

    def get_order_shipments(self, order_id):
        url = self.url(f"/v2/orders/{order_id}/shipments")
        resp = self.client.get(url)

        if resp.status_code != 200:
            resp.raise_for_status()

        try:
            content = resp.json()
        except:
            content = None
        finally:
            return content

    def get_order_shipping_addresses(self, order_id):
        url = self.url(f"/v2/orders/{order_id}/shipping_addresses")
        resp = self.client.get(url)

        if resp.status_code != 200:
            resp.raise_for_status()

        try:
            content = resp.json()
        except:
            content = None
        finally:
            return content

    def get_order_products_ids(self, order_id):
        url = self.url(f"/v2/orders/{order_id}/products")
        resp = self.client.get(url)

        if resp.status_code != 200:
            resp.raise_for_status()

        try:
            content = resp.json()
        except:
            content = []

        order_items = []
        for item in content:
            order_items.append({
                "order_product_id": item.get("id"),
                "quantity": item.get("quantity"),
            })

        return order_items